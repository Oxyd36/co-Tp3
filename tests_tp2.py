from tp2 import *

# on veut pouvoir creer des boites
def test_box_create():
	b = Box()
	return b

assert test_box_create

def test_box_add():
	b = Box()
	b.add("sword")
	b.add("shield")
	return b.get()

assert test_box_add() == ["sword", "shield"]

def test_box_get():
	b = Box()
	b.add("sword")
	return b.get()

assert test_box_get() == ["sword"]

def test_box_contain():
	b = Box()
	b.add("sword")
	assert b.contains("sword") == True
	assert b.contains("shield") == False

def test_box_retirer():
	b = Box()
	b.add("sword")
	b.add("shield")
	b.retirer("sword")
	return b.get()

assert test_box_retirer() == ["shield"]

def test_box_is_open():
	b = Box(True)
	assert b.is_open()
	b = Box(False)
	assert b.is_open() == False

def test_box_close():
	b = Box(True)
	assert b.is_open()
	b.close()
	assert b.is_open() == False

def test_box_open():
	b = Box(False)
	assert b.is_open() == False
	b.open()
	assert b.is_open()

def test_box_action_look():
	b = Box(False)
	assert b.action_look() == "la boite est fermee"
	b = Box(True)
	b.add("sword")
	b.add("shield")
	assert b.action_look() == "la boite contient: sword, shield"

def test_box_set_capacity():
	b = Box(True)
	b.set_capacity(3)
	return b.get_capacity()
	assert test_box_set_capacity() == 3

def test_box_get_capacity():
	b = Box(True)
	return b.get_capacity()
	assert test_box_get_capacity() == None

#_____________Tests pour la class Thing______________:

def test_thing_create():
	t = Thing(3, "sword")
	return t

def test_thing_get_volume():
	t = Thing(3, "sword")
	return t.get_volume()
	assert test_thing_volume() == 3

def test_thing_get_name():
	t = Thing(3, "shield")
	return t.get_name()
	assert test_thing_get_name() == "shield"

def test_thing_set_name():
	t = Thing(3, "shield")
	t.set_name("sword")
	return t.get_name()
	assert test_thing_set_name() == "sword"

def test_thing_set_volume():
	t = Thing(3, "shield")
	t.set_volume(2)
	return t.get_volume()
	assert test_thing_set_volume() == 2

def test_thing_has_name():
	t = Thing(3, "sword")
	return t.has_name("shield")
	assert test_thing_has_name() ==  False
	return t.has_name("sword")
	assert test_thing_has_name()

#___________Retour tests boite_____________:

def test_box_has_room_for():
	b = Box(True, 3)
	s = Thing(3, "sword")
	c = Thing(4, "shield")
	return b.has_room_for(s)
	assert test_box_has_room_for()
	return b.has_room_for(c)
	assert test_box_has_room_for() == False

def test_box_action_add():
	b = Box(False, 3)
	s = Thing(3, "sword")
	c = Thing(4, "shield")
	return b.action_add(c)
	assert test_box_action_add() == False
	b.open()
	assert test_box_action_add() == False
	b.action_add(s)
	return b.get() == ["sword"]
	return b.get_volume == 0

def test_box_find():
	b = Box(False, 3)
	s = Thing(3, "sword")
	c = Thing(4, "shield")
	b.add(s)
	return b.find(s)
	assert test_box_find() == False
	b.open()
	return b.find(c)
	assert test_box_find() == False
	return b.find(s)
	assert test_box_find() == True
