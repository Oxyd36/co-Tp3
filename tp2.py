class Box:
	def __init__(self, is_open = True, capacity = None):
		self._contents = []
		self.condition = is_open
		self.capacity = capacity
	def add(self, truc):
		self._contents.append(truc)
	def get(self):
		return self._contents
	def contains(self, machin):
		return machin in self._contents
	def retirer(self, machin):
		self._contents.remove(machin)
	def is_open(self):
		if self.condition:
			return True
		else:
			return False
	def close(self):
		self.condition = False
	def open(self):
		self.condition = True
	def action_look(self):
		if self.condition:
			return ("la boite contient: "+", ".join(self._contents))
		else:
			return ("la boite est fermee")
	def set_capacity(self, capa):
		self.capacity = capa
	def get_capacity(self):
		return self.capacity
	def has_room_for(self, truc):
		if truc.volume <=  self.capacity:
			return True
		else:
			return False
	def action_add(self, truc):
		if self.condition:
			if self.has_room_for(truc):
				self.set_capacity(self.get_capacity-truc.get_volume())
				self.add(truc)
				return True
			else:
				return False
		else:
			return False
	def find(self, machin):
		if self.condition:
			if self._contains(machin):
				return machin.name
			else:
				return None
		else:
			return None


class Thing:
	def __init__(self, volume = None, name = ""):
		self.volume = volume
		self.name = name
	def get_volume(self):
		return self.volume
	def get_name(self):
		return self.name
	def set_name(self, name):
		self.name = name
	def set_volume(self, volume):
		self.volume = volume
	def has_name(self, name):
		if name == self.name:
			return True
		else:
			return False
